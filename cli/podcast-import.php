<?php

include("../public/pimcore/cli/startup.php");

use \Pimcore\Model\Object\Sermon;



class PodcastImporter
{

    public $parentFolderId = null;

    public $podcastImage = null;

    public $keys = [];

    public $i = 0;

    public function __construct()
    {
        $folder = Pimcore\Model\Object\Folder::getByPath('/sermons');
        $this->parentFolderId = $folder->getId();
    }

    public function importFeed( $url )
    {
        $feed = Zend_Feed_Reader::import($url);

        foreach ($feed as $entry) {
            $data = [
                'title' => $entry->getTitle(),
                'date' => new \Carbon\Carbon( $entry->getDateModified() ),
                'description' => $this->normalizeDescription($entry->getDescription()),
                'image' => $this->getImageAsset(),
                'audioUrl' => $entry->getEnclosure()->url,
                'audioType' => $entry->getEnclosure()->type,
            ];

            $sermon = Sermon::create($data);
            $sermon->setKey( $this->generateKey( $entry ));
            $sermon->setParentId( $this->parentFolderId  );
            $sermon->setPublished(true);
            $sermon->save();
        }
    }

    protected function generateKey( $entry )
    {

        $pubDate = new \Carbon\Carbon( $entry->getDateModified() );
        $title = preg_replace('/[^a-zA-Z0-9]/i', '_', $entry->getTitle() );

        $newKey = $pubDate->format('Ymd') . '-' . substr($title, 0, 20);
        if( in_array( $newKey, $this->keys ) ){
            $this->i++;
            $newKey .= $this->i;
        } else {
            $this->i = 0;
        }

        return $newKey;
    }

    public function getImageAsset()
    {
        if( !$this->podcastImage ){
            $this->podcastImage = Pimcore\Model\Asset::getByPath("/podcast-art.png");
        }
        return $this->podcastImage;
    }

    public function normalizeDescription( $raw )
    {
        $normalized = preg_replace('#<br\s*?/?>#i', "\n", $raw);
        return strip_tags($normalized);
    }

}

$pi = new PodcastImporter();
$pi->importFeed('http://mkenenske.libsyn.com/rss');