<?php

use Website\Controller\Action;
use Pimcore\Model\Object\Sermon;

class DefaultController extends Action {

    public function defaultAction() {
        $this->enableLayout();
    }

    public function homeAction()
    {
        $this->enableLayout();

        $sermonList = new Sermon\Listing();
        $sermonList->setOrderKey('date');
        $sermonList->setOrder('DESC');
        $sermonList->setLimit(1);

        $this->view->sermon = current($sermonList->load());
    }

}
