<?php

use Website\Controller\Action;

class mediaController extends Action {

    public function defaultAction() {
        $this->enableLayout();

        // Tag filter
        $this->view->tagId = $this->_getParam('tag');
        if ($this->view->tagId) {
            $otherConditions = $this->getTagCondition($this->view->tagId);
        }

        $sermonList = new \Pimcore\Model\Object\Sermon\Listing();
        if( $this->view->tagId ){
            $sermonList->setCondition($otherConditions);
        }
        $sermonList->setOrderKey('date');
        $sermonList->setOrder('DESC');

        $paginator = \Zend_Paginator::factory($sermonList);
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(10);
        $this->view->paginator = $paginator;



        // Tags
        $tags = $this->getObjectTypeTags($sermonList->getClassId());

        $this->view->tags = $tags;
        $this->view->postType = 'recipe';
    }
    protected function getDb()
    {
        return \Pimcore\Db::get();
    }

    protected function getObjectTypeTags($classId)
    {
        $tagSql = sprintf('SELECT DISTINCT name, id 
                    FROM tags 
                    INNER JOIN (tags_assignment 
                      INNER JOIN object_store_%d ON object_store_%d.oo_id = tags_assignment.cid) 
                  ON tags_assignment.tagid = tags.id', $classId, $classId);
        $tagQuery = $this->getDb()->query($tagSql);
        return $tagQuery->fetchall();
    }
    protected function getTagCondition($tagId)
{
    $tagSql = 'o_id IN (SELECT cId FROM tags_assignment WHERE ctype = \'object\' AND tagid = ?)';
    return $this->getDb()->quoteInto($tagSql, $tagId);
}
}
