<?php

use Website\Controller\Action;

class AboutController extends Action {

    public function defaultAction() {
        $this->enableLayout();

        $personList = new \Pimcore\Model\Object\Person\Listing();
        $personList->setOrderKey('sortOrder');
        $personList->setOrder('DESC');
        $this->view->person = $personList->load();
    }

}
