<?php
$this->layout()->setLayout('home');
?><!doctype html>
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/jpeg" href="/img/favicon.jpg" />

    <?php

    // get root node if there is no document defined (for pages which are routed directly through static route)
    if(! $this->document instanceof \Pimcore\Model\Document\Page) {
        $this->document = \Pimcore\Model\Document\Page::getById(1);
    }

    // get the document which should be used to start in navigation | default home
    $navStartNode = $this->document->getProperty("navigationRoot");
    if(!$navStartNode instanceof \Pimcore\Model\Document\Page) {
        $navStartNode = \Pimcore\Model\Document\Page::getById(1);
    }
    $mainNavigation = $this->pimcoreNavigation($this->document, $mainNavStartNode, null, function ($page, $document) {
        $page->setCustomSetting("subListClass", $document->getProperty("subListClass"));
        $page->setCustomSetting("title", $document->getTitle());
        $page->setCustomSetting("description", $document->getDescription());
    });
    $mainNavigation->menu()->setPartial("/navigation/partials/navigation.php");
    /*
    $navigation->addPage([
        'order' => -1,
        'uri' => '/',
        'label' => 'Home',
        'title' => 'Home page',
    ]);
    */

    if ($this->document->getTitle()) {
        // use the manually set title if available
        $this->headTitle()->set($this->document->getTitle());
    }
    if ($this->document->getDescription()) {
        // use the manually set description if available
        $this->headMeta()->appendName('description', $this->document->getDescription());
    }
    $this->headTitle()->append("United Redeemed");
    $this->headTitle()->setSeparator(" : ");
    echo $this->headTitle();
    echo $this->headMeta();
    echo $this->headLink();
    ?>
    <script src="https://use.fontawesome.com/6a0b2f6a34.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://use.typekit.net/mtu3rxb.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <link rel="stylesheet" href="/css/font-awesome.min.css">

    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/css/style.min.css">

</head>
<body>
<div canvas="container">
    <div class="above-footer">
        <section class="main-highlight">
            <a class="toggle-mobile-nav-container" href="#">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                     x="0px" y="0px" width="128px" height="86px" viewBox="0 0 30 24"
                     enable-background="new 0 0 30 24" xml:space="preserve">
                        <rect x="0" width="32" height="5"/>
                        <rect x="0" y="9.5" width="32" height="5"/>
                        <rect x="0" y="19" width="32" height="5"/>
                        </svg></a>

            <div class="nav-and-logo">
                <h1 class="site-logo"><a href="/"><img src="/img/logo.png" alt="United Redeemed" /></a></h1>

                <nav class="main-nav">
                    <ul>
                    <?php echo $mainNavigation->render(); ?>
                    </ul>
                    <div class="search-wrapper home">
                        <form action="/search" class="search-block-form">
                            <input type="text" name="query">
                            <input type="submit" class="form-submit" value="Search">
                        </form>
                    </div>
                    <a class="new-btn" href="/about/newtoUR"><span>New?</span><span>Start here</span></a>
                </nav>
            </div>

            <div class="call-to-action-container">
                <div class="call-to-action">
                    <?= $this->areablock('call-to-action'); ?>
                </div>
            </div>

            <div class="main-highlight-footer">
                <div class="schedule">
                    <?= $this->wysiwyg('schedule'); ?>
                    <a href="#" class="more-link"><?= $this->input('logistics-message'); ?></a>
                </div>
                <div class="podcast">
                    <div class="podcast-episode">
                        <h2>&ldquo;<?= $this->sermon->getTitle(); ?>&rdquo;</h2>
                        <audio controls>
                            <source src="<?= $this->sermon->getAudioUrl(); ?>" type="<?= $this->sermon->getAudioType(); ?>">
                            <a href="" class="play-button"><i class="fa fa-play"></i> Play</a>
                        </audio>
                        <a href="/media"><img src="/img/icon/podcast-circle.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </section>
        <!-- SEARCH_START -->
        <?= $this->layout()->content; ?>
        <!-- SEARCH_END -->
    </div>
    <?= $this->inc('/snippets/footer'); ?>
</div>
<div off-canvas="mobile-nav-container right reveal">
    <div class="mobile-nav-content">
        <a href="#" class="mobile-nav-container-close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                 y="0px" width="30px" height="30px" viewBox="0 0 30 30" enable-background="new 0 0 30 30"
                 xml:space="preserve">
                <path fill="#fff"
                      d="M15,2c7.168,0,13,5.832,13,13c0,7.168-5.832,13-13,13C7.832,28,2,22.168,2,15C2,7.832,7.832,2,15,2 M15,0 C6.715,0,0,6.716,0,15c0,8.285,6.715,15,15,15c8.284,0,15-6.715,15-15C30,6.716,23.284,0,15,0L15,0z"/>
                <line fill="none" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"
                      stroke-miterlimit="10" x1="9" y1="9" x2="21" y2="21"/>
                <line fill="none" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"
                      stroke-miterlimit="10" x1="21" y1="9" x2="9" y2="21"/>
                </svg>
        </a>
        <nav id="slidebars-nav">
            <?php
            /** @var \Pimcore\View\Helper\PimcoreNavigation $mobileNavigation */
            $mobileNavigation = $this->pimcoreNavigation($this->document, $navStartNode, 'mobile-nav');
            $navigation = $mobileNavigation->getContainer();

            echo $this->navigation()->menu()
                ->setUlClass('primary-navigation')
                ->renderMenu($navigation, array("maxDepth" => 2, 'parentClass' => 'parent', 'renderParentClass' => true));
            ?>
        </nav>
    </div>
</div>
<script src="/js/slidebars/slidebars.min.js"></script>
<script>
    (function ($) {
        // Initialize Slidebars
        var controller = new slidebars();
        controller.init();

        // Open button
        $('.toggle-mobile-nav-container').on('click', function (event) {
            // Stop default action and bubbling
            event.stopPropagation();
            event.preventDefault();

            // Toggle the Slidebar with id 'id-1'
            controller.toggle('mobile-nav-container');
        });

        // Close button
        $('.mobile-nav-container-close').on('click', function (event) {
            // Stop default action and bubbling
            event.stopPropagation();
            event.preventDefault();

            // Toggle the Slidebar with id 'id-1'
            controller.close('mobile-nav-container');
        });
    })($);
</script>
<!--
<script>

    /** THIS USES THE CATHOLIC SCIENTISTS CODE (dkidd 1/10/17)


     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-86900144-1', 'auto');
     ga('send', 'pageview');

     **?

</script>
-->
</body>
</html>