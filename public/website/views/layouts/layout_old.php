<!doctype html>
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
        // portal detection => portal needs an adapted version of the layout
        $isPortal = false;
        if ($this->getParam("controller") == "content" && $this->getParam("action") == "portal") {
            $isPortal = true;
        }


        // get root node if there is no document defined (for pages which are routed directly through static route)
        if (!$this->document instanceof \Pimcore\Model\Document\Page) {
            $this->document = \Pimcore\Model\Document\Page::getById(1);
        }

        // get the document which should be used to start in navigation | default home
        $navStartNode = $this->document->getProperty("navigationRoot");
        if (!$navStartNode instanceof \Pimcore\Model\Document\Page) {
            $navStartNode = \Pimcore\Model\Document\Page::getById(1);
        }
        /** @var \Pimcore\View\Helper\PimcoreNavigation $mainNavigation */
        $mainNavigation = $this->pimcoreNavigation($this->document, $navStartNode);
        $navigation = $mainNavigation->getContainer();
        $navigation->addPage([
            'order' => -1,
            'uri' => '/',
            'label' => 'Home',
            'title' => 'Home page',
        ]);

        if ($this->document->getTitle()) {
            // use the manually set title if available
            $this->headTitle()->set($this->document->getTitle());
        }
        if ($this->document->getDescription()) {
            // use the manually set description if available
            $this->headMeta()->appendName('description', $this->document->getDescription());
        }
        $this->headTitle()->append("United Redeemed");
        $this->headTitle()->setSeparator(" : ");
        echo $this->headTitle();
        echo $this->headMeta();
        echo $this->headLink();
        ?>
        <script src="https://use.typekit.net/kkb2xwn.js"></script>
        <script>try {
                Typekit.load({async: true});
            } catch (e) {
            }</script>

        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/style.min.css">

    </head>
    <body>
        <div class="above-footer">
            <header class="default-page-header">
                <div class="container">
                    <h1 class="site-logo"><a href="/"><img src="/img/logo.png" alt="United Redeemed" /></a></h1>
                    <nav class="main-nav">

                        <?= $mainNavigation->menu()->renderMenu($navigation, ["maxDepth" => 2]); ?>
                    </nav>
                </div>
            </header>

            <!-- SEARCH_START -->
            <?= $this->layout()->content; ?>
            <!-- SEARCH_END -->
        </div>
        <?= $this->inc('/snippets/footer'); ?>
        <!--
        <script>
        
            /** THIS USES THE CATHOLIC SCIENTISTS CODE (dkidd 1/10/17)
        
        
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
             ga('create', 'UA-86900144-1', 'auto');
             ga('send', 'pageview');
        
             **?
        
        </script>
        -->
    </body>
</html>