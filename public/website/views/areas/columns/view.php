<div class="column-widget">
    <?php if ($this->editmode) { ?> 
        <p>Set first column width (6 is default, 50/50 split). Second column set automatically.
        </p>
        <?php
        echo $this->numeric("width", array(
            "width" => 300,
            "minValue" => 2,
            "maxValue" => 10,
                )
        );
    }
    $width = $this->numeric("width")->getValue();
    $width2 = 12 - $width;
    ?>
    <div class="row">
        <div class="column col-md-<?php
        if ($this->numeric("width")->isEmpty()) {
            echo "6";
        } else {
            echo $width;
        }
        ?>">
                 <?php echo $this->areablock('column1') ?>
        </div>
        <div class="column col-md-<?php
             if ($this->numeric("width")->isEmpty()) {
                 echo "6";
             } else {
                 echo $width2;
             }
             ?>">
                 <?php echo $this->areablock('column2') ?>
        </div>
    </div>
</div>
