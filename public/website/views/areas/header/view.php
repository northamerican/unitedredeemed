<header>
    <?= $this->template('_includes/backgroundDivOpenTag.php',[
        'editmode' => $this->editmode,
        'imgName' => 'background-image',
        'classes' => 'generic-banner',
        'overlayControls' => true,
    ]); ?>
        <h2 style="color:<?= $this->select('title-color')->getData() === 'white' ? '#fff' : '#000';
        ?>"><?= $this->input("title"); ?></h2>
    </div>
</header>