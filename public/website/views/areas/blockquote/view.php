<section class="area-blockquote" style="margin-top:20px;">

    <blockquote>
        <p><?= $this->input("quote"); ?></p>
        <footer class="blockquote-attribution"><?= $this->input("author"); ?></footer>
    </blockquote>

</section>

