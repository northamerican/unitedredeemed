<?php if ($this->editmode) { ?>   
    <section class="qa-row">
        <h2>Question and Answer Row Instructions</h2>
        <p>Use the controls below to add a row of questions and answers. You may
            provide up to three question and answer sets per row. You do not need
            to fill in every set, but be sure to fill in sets from left to right.</p>
        <div class="container">
            <div class="row">
                <?php for( $i=0; $i<3; $i++ ): ?>
                    <div class="col-md-4" style="border:black solid 1px">
                        <h3>Question</h3>
                        <?php echo $this->wysiwyg("question-$i"); ?>
                        <h3>Answer</h3>
                        <?php echo $this->wysiwyg("answer-$i"); ?>                   
                        <h3>Link (optional)</h3>
                        <?php echo $this->link("link-$i") ?>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </section>
<?php } else { ?>
    <div class="qa-row">
        <div class="row">
            <?php for( $i=0; $i<3; $i++ ):
                if( $this->wysiwyg("question-$i")->isEmpty() ){
                    break;
                }
                ?>
                <div class="col-md-4 <?php
                    if( $i === 0 ){
                        if( $this->wysiwyg('question-1')->isEmpty() ){
                            echo 'col-md-offset-4';
                        } elseif( $this->wysiwyg('question-2')->isEmpty() ){
                            echo 'col-md-offset-2';
                        }
                    }
                ?>">
                    <div class="qa-set">
                        <div class="question-wrapper"><?php echo $this->wysiwyg("question-$i"); ?></div>
                        <div class="answer-wrapper">
                            <?php echo $this->wysiwyg("answer-$i"); ?>
                            <?php if (!$this->link("link-$i")->isEmpty()) { ?>
                                <?php echo $this->link("link-$i") ?> <i class="fa fa-caret-right"></i>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
<?php } ?>
