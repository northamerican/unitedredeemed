<div class="area-image">
    <a href="<?= $this->image("image")->getSrc(); ?>">
        <?= $this->image("image", [
            "thumbnail" => "galleryThumbnail"
        ]); ?>
    </a>
</div>