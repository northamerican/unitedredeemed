<?php if ($this->editmode): ?>
    <div class="admin-outline-box">
<div class="admin-instructions-container">
    <h3>Add background photo here</h3>
    <?= $this->href('section-photo', [
        'types' => ['asset'],
        'subtypes' => [
            'asset' => ['image']
        ],
    ]);
    ?>
    <h3>Add contents to area here</h3>
</div>
<div class="section-paper-body-wrapper">
    <?= $this->areablock('default-section-body'); ?>
</div>
    </div>

<?php else: ?>
<div class="section-body-wrapper" style="background: linear-gradient(rgba( 0, 0, 0, 0.4), rgba(0,0,0,0.44))<?php
        if ($this->href("section-photo")->getElement() ){
            ?>, url(<?= $this->href("section-photo")->getElement()->getThumbnail('full-background')->getPath(); ?>) no-repeat center center<?php
        }
        ?>; background-size:cover">
    <div class="section-paper-body-wrapper">
        <?= $this->areablock('default-section-body'); ?>
    </div>
</div>
<?php endif; ?>