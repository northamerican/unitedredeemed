<?php
$this->headScript()->appendFile('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js');
$id = "accordion-" . uniqid();
?>
<div class="area-text-accordion">
    <div class="panel-group" id="<?= $id ?>">
        <?php while($this->block("accordion")->loop()) {
                $entryId = $id . "-" . $this->block("accordion")->getCurrent();
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#<?= $id ?>" href="#<?= $entryId ?>">
                            <?= $this->input("headline") ?>
                        </a>
                    </h4>
                </div>
                <div id="<?= $this->editmode ? "" : $entryId ?>" class="panel-collapse collapse <?= ($this->editmode || $this->block("accordion")->getCurrent() == 0) ? "in" : "" ?>">
                    <div class="panel-body">
                        <?= $this->wysiwyg("text") ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

