<?php
if (!$this->editmode) {
    $this->headLink()->appendStylesheet('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css');
  //  $this->headScript()->appendFile('https://code.jquery.com/jquery-1.12.4.min.js');
    $this->headScript()->appendFile('https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.1/imagesloaded.pkgd.min.js');
    $this->headScript()->appendFile('https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.1/masonry.pkgd.min.js');
    $this->headScript()->appendFile('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js');

} ?>
<div class="area-gallery-single-images">

    <div class="grid">
        <div class="grid-sizer">
            <?php
            $block = $this->block("gallery");
            $count = $this->block("gallery")->getCount();
            while ($block->loop()) {
                ?>
                <div class="grid-item">
                    <?php
                    if ($this->editmode) {
                        echo $this->image('image', array(
                            "width" => 500,
                            'aspectratio' => true,
                            'attributes' => [
                                'class' => 'gallery-image',
                            ],
                        ));

                        echo $this->input('caption', [
                            width => 500,
                        ]);
                    } else {
                        ?>
                        <a class="fancybox" rel="gallery" href="<?php echo $this->image('image')->getSrc(); ?>">
                            <img class="gallery-image" src="<?php
                                 echo $this->image('image')->getThumbnail([
                                     "width" => 500,
                                     'aspectratio' => true,
                                 ]);
                                 ?>"></a>
                            <p><?= $this->input('caption'); ?></p>
                     <?php } ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<script>
    $(function(){
// initialize Masonry after all images have loaded
        $('.area-gallery-single-images').imagesLoaded(function () {
            $('.grid').masonry({
                // options
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
            });
        });
        $(".fancybox").fancybox();
    });
</script>