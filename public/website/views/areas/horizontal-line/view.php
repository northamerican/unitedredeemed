<?php if ($this->editmode) { ?>
    Thickness: <?php echo $this->numeric('thickness'); ?>
    <div style="background-color:#FFFFFF;">
        <?php echo $this->multiselect("color", array(
        "width" => 200,
        "height" => 100,
        "store" => array(
            array("green", "green"),
            array("yellow", "yellow"),
            array("blue", "blue"),
            array("gray", "gray"),
            array("red", "red"),
            array("black", "black"),
            array("white", "white"),
        )
    ))
    ?>
    </div>
    <hr class="">
<?php } else { ?>
    <hr class="hr-brick <?php echo $this->multiselect("color") ?>" style="border-top-width:<?php echo $this->numeric('thickness'); ?>px">
<?php } ?>

