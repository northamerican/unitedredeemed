<?php if ($this->editmode):
    $overlayControls = isset( $this->overlayControls ) && $this->overlayControls;
    $colWidths = $overlayControls ? 4 : 6;

    ?>
    <div class="admin-instructions-container"<?php if( !$overlayControls ){ echo ' style="max-width:500px"'; } ?>>
        <h2>Block with Image Background</h2>
        <div class="row">
            <div class="col-md-<?= $colWidths ?>">
                <p>To get started using this block, choose an image using the image-selection tool on the right. The
                    next time you load this admin page (after saving the document), the chosen image will be set as
                    the background of the area below.</p>
            </div>
            <div class="col-md-<?= $colWidths ?>">
                <dl>
                    <dt>Image</dt>
                    <dd><?= $this->image($this->imgName); ?></dd>
                </dl>
            </div>
            <?php if( $overlayControls ): ?>
            <div class="col-md-<?= $colWidths ?>">
                <dl>
                    <dt>Overlay Opacity (0 - 100)</dt>
                    <dd><?php
                        if( $this->numeric('opacity')->number == '' ){
                            $this->numeric('opacity')->number = 60;
                        }
                        echo $this->numeric("opacity", ['minValue' => 0, 'maxValue' => 100]);

                        ?></dd>

                    <dt>Overlay Color</dt>
                    <dd><?= $this->select('overlay-color', [ 'store' => [['none', 'None'], ['black', 'Black'], ['white', 'White']]]); ?></dd>

                    <dt>Title Color</dt>
                    <dd><?= $this->select('title-color', [ 'store' => [['black', 'Black'], ['white', 'White']]]); ?></dd>

                    <dt>Top paint stripe?</dt>
                    <dd><?= $this->select('top-paint-stripe', [ 'store' => [['no', 'No'], ['yes', 'Yes']]]); ?></dd>
                </dl>
            </div>
            <?php endif; ?>
        </div>

    </div>
<?php endif;

$overlay = '';
if( $this->select('top-paint-stripe')->getData() === 'yes' ){
    $overlay = 'url(/img/paint-stripe2.png) no-repeat center -60px,';
}
$opacity = isset($this->opacity) ? $this->opacity : $this->numeric('opacity')->number / 100;
$rgbNumbers = isset($this->rgbNumbers ) ? $this->rgbNumbers : $this->select('overlay-color')->getData() === 'black' ? '0,0,0' : '255,255,255';
if( $opacity > 0 && $this->select('overlay-color')->getData() !== 'none' ){
    $overlay .= " linear-gradient(rgba($rgbNumbers,$opacity),rgba($rgbNumbers,$opacity)), ";
}
?>
<div class="<?php if( !empty( $this->classes ) ){ echo $this->classes; }?>"
     style="<?php if( !$this->image($this->imgName)->isEmpty() ): ?>
            background:<?= $overlay ?> url(<?= $this->image($this->imgName)->getThumbnail(array(
                        'width' => 1280,
                        'aspectratio' => true
                    ))?>) 50% 50% / cover no-repeat;
            <?php endif; ?>
    <?php if( !empty($this->additionalStyles) ){ echo $this->additionalStyles; } ?>">