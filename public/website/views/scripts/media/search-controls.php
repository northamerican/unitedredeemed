<div class="teaser-search-wrapper">
    <form action="<?php
    if( $this->postType === 'blogpost' ){
        echo '/blog/search';
    }
    ?>" method="get" class="search-block-form">

        <div class="radio-controls">
            <label class="radio-inline">
                <input type="radio" id="blog-radio" name="post-type" value="blog"<?php if( $this->postType === 'blogpost' ){ ?> checked="checked"<?php } ?> /> Blog posts
            </label>


            <label class="radio-inline">
                <input type="radio" id="recipe-radio" name="post-type" value="recipe"<?php if( $this->postType === 'recipe' ){ ?> checked="checked"<?php } ?>  /> Recipes
            </label>
        </div>

        <hr/>

        <input type="text" name="search">
        <input type="submit" class="form-submit" value="search">
    </form>
</div>

<div class="filter-title"><?= $this->input('tag-filter-title'); ?></div>
<div class="tags-wrapper">
    <?php foreach($this->tags as $tag): ?>
        <a class="tag-name" href="?tag=<?= $tag['id']; ?>" data-tag-id="<?= $tag['id']; ?>"><?= $tag['name']; ?></a>
    <?php endforeach; ?>
</div>

<?php if( isset( $this->ingredients ) ): ?>
    <div class="filter-title"><?= $this->input('ingredient-filter-title'); ?></div>
    <div class="ingredients-wrapper">
        <?php foreach ($this->ingredients as $ingredient): ?>
            <a class="tag-name" href="/blog/recipe-search?ingredient=<?= $ingredient->geto_id(); ?>"><?php echo $ingredient->getName(); ?></a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<script>
    $(function(){
        $('.search-block-form input[type=radio]').change(function(){
            console.log('checked');
            if( $(this).is(':checked') ){
                var urlBase = '/' + $(this).val() + '/search';

                $('.tags-wrapper .tag-name').each(function(){
                    $(this).attr('href', urlBase + '?tag=' + $(this).data('tagId') );
                });

                $('.search-block-form').attr('action', urlBase );
            }
        });
    });
</script>