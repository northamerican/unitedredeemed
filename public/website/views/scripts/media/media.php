<?php
$this->layout()->setLayout('layout');
$this->headLink()->appendStylesheet('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css');
$this->headScript()->appendFile('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js');
?>

<div class="media-container-wrapper">
    <?= $this->areablock('pre-container'); ?>
    <div class="container">
        <h2><?= $this->input('media-header'); ?></h2>
        <div class="row">
            <div class="sermons-wrapper col-md-9">
                <?php $i = 0;
                foreach ($this->paginator as $paginator): ?>
                    <?php $i++;
                    if ($paginator->getImage()) {
                        $background = 'background: linear-gradient(rgba( 0, 0, 0, 0.4), rgba(0,0,0,0.44)), url(' . $paginator->getImage()->getThumbnail('sermon-background')->getPath() . ') center center no-repeat;';
                    } else {
                        $background = 'background: transparent url(/img/concrete-cross.jpg) center center no-repeat;';
                    } ?>
                    <div class="sermon <?php echo($i % 2 ? 'dark' : 'light'); ?>"
                         style="<?php echo($i % 2 ? $background : ''); ?>">
                        <div class="page-edges">
                            <h3 class="sermon-title"><?= $paginator->getTitle(); ?></h3>
                            <span class="sermon-speaker"><?php if ($paginator->getSpeaker()) {
                                    echo $paginator->getSpeaker();
                                }; ?></span>
                            <span class="sermon-date"><?php echo date_format($paginator->getDate(), "M d, Y"); ?></span>
                            <div class="sermon-description"><?php echo $paginator->getDescription(); ?></div>
                            <?php
                            if ($paginator->getVideo()) {
                                $type = $paginator->getVideo()->getType();
                                $id = $paginator->getVideo()->getData();


                                if ($type == "vimeo") {
                                    $videoSrc = 'https://player.vimeo.com/video/' . $id . '?autoplay=1';
                                } else {
                                    $videoSrc = '//www.youtube.com/embed/' . $id . '?autoplay=1';
                                }

                                $attributes['href'] = $videoSrc;
                                $attributes['class'] = 'link-btn fancybox fancybox.iframe';
                                $attributes['target'] = '_blank';
                                $attributes['data-youtube-id'] = $id;
                                ?>
                                <a<?php
                                foreach ($attributes as $key => $val) {
                                    echo ' ' . $key . "=\"$val\"";
                                }
                                ?>>Video </a>
                            <?php }
                            if ($paginator->getAudioUrl()) {
                                ?>
                                <a class="link-btn" target="_blank" href="<?= $paginator->getAudioUrl(); ?>">Audio</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <!-- pagination start -->
                <?=
                $this->paginationControl($this->paginator, 'Sliding', '_includes/paging.php', [
                    'urlprefix' => $this->document->getFullPath() . '?page=',
                    'appendQueryString' => true
                ]);
                ?>
                <!-- pagination end -->
            </div>

            <div class="tags-wrapper col-md-3">
                <div class="row-title">
                    <h3>
                        <?= $this->input('tag-title'); ?>
                    </h3>
                </div>
                <?php foreach ($this->tags as $tag): ?>
                    <a class="tag-name" href="?tag=<?= $tag['id']; ?>"><?= $tag['name']; ?></a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".fancybox").fancybox();
    });
</script>