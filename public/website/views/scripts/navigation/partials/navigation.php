<?php foreach ($this->container as $page) { ?>
    <?php if ($page->isVisible()) {
        $hasVisibleChildren = array_reduce( $page->getPages(), function( $carry, $p ){
            return $carry || $p->isVisible();
        });
        ?>
        <li class="<?php if ($page->getActive(true)) { ?>active<?php } ?>">
            <a href="<?= $page->getUri() ?>" <?php if ($page->getTarget()) {?>  target="<?= $page->getTarget()?>" <?php }?>><?= $page->getLabel() ?>
                <?php if( $hasVisibleChildren ): ?>
                <div class="outer-indicator-triangle-up "></div>
                <div class="indicator-triangle-up "></div>
                <?php endif; ?>
            </a>
            <?php if( $hasVisibleChildren ): ?>
            <div class="menu-wrapper">
                <ul class="<?= $page->getCustomSetting("subListClass") ?>" role="menu">
                    <li class="<?php if ($page->getActive(true)) { ?>active<?php } ?>">
                        <a href="<?= $page->getUri() ?>" <?php if ($page->getTarget()) {?>  target="<?= $page->getTarget()?>" <?php }?>><?= $page->getLabel() ?></a>
                        <div class="menu-description">
                            <?= $page->getCustomSetting("description"); ?>
                        </div>
                    </li>

                    <?php $this->template("/navigation/partials/sub.php", [ "container" => $page->getPages()]); ?>
                </ul>
            </div>
            <?php endif; ?>
        </li>
    <?php } ?>
<?php } ?>