<?php foreach( $this->container as $page ){ ?>
    <?php if($page->isVisible()){ ?>
         <li class="<?php if( $page->getActive(true) ){ ?>active<?php } ?>">
          <a href="<?= $page->getUri() ?>" <?php if ($page->getTarget()) {?>  target="<?= $page->getTarget()?>" <?php }?>><?= $page->getLabel() ?></a>
          <div class="menu-description">
              <?= $page->getCustomSetting("description"); ?>
          </div>
     </li>
    <?php } ?>
<?php } ?>