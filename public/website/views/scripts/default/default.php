<?php $this->layout()->setLayout('layout'); ?>
<div class="default-container-wrapper">
    <?= $this->areablock('pre-container'); ?>
    <div class="container">
        <div class="default-body-wrapper">
            <?= $this->areablock('default-body'); ?>
        </div>
    </div>
    <?= $this->areablock('post-container'); ?>
</div>