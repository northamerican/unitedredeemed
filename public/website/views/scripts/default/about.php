<!DOCTYPE html>
<?php $this->layout()->setLayout('layout');
$this->headScript()->appendFile('/js/min/custom-modal-min.js');
?>

<div class="about-container-wrapper">
    <?= $this->areablock('pre-container'); ?>
    <div class="container">
        <div class="about-us-intro">
            <div class="row">
                <div class="col-md-6 photo-wrapper">
                    <?php
                    if ($this->editmode) {
                        echo $this->image('about-us-intro');
                    } else {
                    if (!$this->image('about-us-intro')->isEmpty()) {
                        echo $this->image('about-us-intro')->getThumbnail([
                            'width' => 520,
                        ])->getHtml([], ["width","height"]);;
                    }
                     }?>
                </div>
                <div class="col-md-6 text-wrapper">
                    <?= $this->wysiwyg('about-us-intro-text'); ?>
                </div>
            </div>
        </div>
        <div class="people">
            <div class="row">
                <?php foreach ($this->person as $i => $person): ?>
                    <a class="person-modal-link" data-toggle="modal"
                       data-target="#modal-<?= $person->getID(); ?>">
                        <div class="staff-person-wrapper col-md-4">

                            <div class="staff-person"
                                 style="background:linear-gradient(rgba( 0, 0, 0, 0.4), rgba(0,0,0,0.44)), url('<?= $person->getPhoto()->getThumbnail('staff-portrait')->getPath(); ?>') center center no-repeat; background-size:cover;">
                                <h3 class="person-name"><?= $person->getName(); ?></h3>
                                <span class="person-title"><?= $person->getTitle(); ?></span>
                                <span class="person-email"><?php if ($person->getEmail()) {
                                        echo $person->getEmail();
                                    }; ?></span>
                            </div>
                            <div class="person-click">Click For Bio</div>

                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="people-modals">
            <?php foreach ($this->person as $person): ?>
                <div class="modal fade" id="modal-<?= $person->getID(); ?>" tabindex="-1" role="dialog"
                     aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="col-md-offset-1 modal-about-staff-person">
                                    <h3 class="person-name"><?= $person->getName(); ?></h3>
                                    <span class="person-title"><?= $person->getTitle(); ?></span>
                                    <span class="person-email"><?php if ($person->getEmail()) {
                                            echo $person->getEmail();
                                        }; ?></span>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-1">
                                        <?php if ($person->getPhoto()) { ?>
                                            <div class="image-wrapper">
                                                <?php
                                                if ($person->getPhoto()) {
                                                    echo $person->getPhoto()->getThumbnail('staff-portrait')->getHtml();
                                                } else {
                                                    ?><img src="/img/person-no-photo.png" alt=""/><?php
                                                }
                                                ?>
                                            </div>
                                        <?php } ?>

                                    </div>
                                    <div class="col-md-7">
                                        <?= $person->getBio(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
    <div class="board-body-wrapper">
        <div class="container">
            <div class="row">
                <h2>
                    <?= $this->input('board-header'); ?>
                </h2>
                <div class="col-md-4 col-md-offset-2">
                    <?= $this->wysiwyg('board-body-1'); ?>

                </div>
                <div class="col-md-4">
                    <?= $this->wysiwyg('board-body-2'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php if ($this->editmode): ?>
        <h3>Add volunteer photo here</h3>
        <?= $this->href('volunteer-photo', [
            'types' => ['asset'],
            'subtypes' => [
                'asset' => ['image']
            ],
        ]);

    ?><div>
        <div>
<?php else: ?>
    <div class="volunteer-body-wrapper" style="background: linear-gradient(rgba( 0, 0, 0, 0.4), rgba(0,0,0,0.44)),
            url(<?php if ($this->href("volunteer-photo")->getElement()): ?>
        <?= $this->href("volunteer-photo")->getElement()->getThumbnail('full-background')->getPath(); ?>
    <?php endif; ?>)
            center center no-repeat; background-size:cover">
        <div class="paper-body-wrapper"
             style="background-image: url(/img/paint-stripe-bottom.png), url(/img/paint-stripe-top.png);">
<?php endif;?>

            <div class="container">
                <div class="row">
                    <h2>
                        <?= $this->input('volunteer-header'); ?>
                    </h2>
                    <div class="col-md-4 col-md-offset-2">
                        <?= $this->wysiwyg('volunteer-body-1'); ?>

                    </div>
                    <div class="col-md-4">
                        <?= $this->wysiwyg('volunteer-body-2'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>