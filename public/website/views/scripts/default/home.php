<?php $this->layout()->setLayout('home'); ?>
<section class="activity">
    <h2 class="activity-title"><?= $this->input('activity-title'); ?></h2>

    <div class="activity-notices">
        <?= $this->area("news-highlights", ["type" => "newshighlights"]); ?>
        <?= $this->area("coming-events", ["type" => "comingevents"]); ?>
    </div>
</section>

<section class="ministries">
    <h2 class="ministry-title"><?= $this->input('ministry-title'); ?></h2>

    <?php while($this->block("ministry-teasers")->loop()): ?>
    <div class="ministry">
        <h3><?= $this->input('ministry-name'); ?></h3>

        <div class="content">
            <?php if( $this->editmode ): ?>
                <?= $this->wysiwyg('ministry-teaser-text'); ?>
            <?php endif; ?>
            <aside>
                <?= $this->link("ministry-more", ["class" => "btn btn-outline"]); ?>
            </aside>
            <?php if( !$this->editmode ): ?>
                <?= $this->wysiwyg('ministry-teaser-text'); ?>
            <?php endif; ?>
        </div>
    </div>
    <?php endwhile; ?>

</section>