<?php $this->layout()->setLayout('layout'); ?>
<div class="container default-container">
    <section class="default-areablock">
        <?= $this->areablock('pre-search') ?>
    </section>
        <section class="search-block">
            <?= $this->action("find", "frontend", "SearchPhp", array( 'omitJsIncludes' => true)) ?>
        </section>
    <section class="default-areablock">
        <?= $this->areablock('post-search') ?>
    </section>
</div>
