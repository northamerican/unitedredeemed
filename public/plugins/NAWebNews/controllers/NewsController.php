<?php

use Pimcore\Model\Object\News;
use NAWebNews\Controller\Action;

class NAWebNews_NewsController extends Action
{
    public function indexAction()
    {
        $this->view->paginator = $this->getPaginator();
    }

    public function detailAction()
    {
        $key = $this->getParam('key');
        $newsList = new News\Listing();
        $newsList->setCondition('o_key=?', $key)
            ->setLimit(1);
        $newsItem = current($newsList->load());

        if (!$newsItem) {
            throw new Zend_Controller_Router_Exception('This page does not exist', 404);
        }
        $this->view->newsItem = $newsItem;

        // Previous item
        $prevList = new News\Listing();
        $prevList->setCondition('IF(published_date = ?, o_creationDate < ?, published_date < ?)',
            [
                $newsItem->getPublished_date()->timestamp,
                $newsItem->getCreationDate(),
                $newsItem->getPublished_date()->timestamp,
            ]);
        $prevList->setOrderKey(['published_date', 'o_creationDate'])
            ->setOrder('DESC')
            ->setLimit(1);
        $this->view->prevItem = current($prevList->load());

        // Next item
        $nextList = new News\Listing();
        $nextList->setCondition('IF(published_date = ?, o_creationDate > ?, published_date > ?)',
            [
                $newsItem->getPublished_date()->timestamp,
                $newsItem->getCreationDate(),
                $newsItem->getPublished_date()->timestamp,
            ]);
        $nextList->setOrderKey(['published_date', 'o_creationDate'])
            ->setOrder('ASC')
            ->setLimit(1);
        $this->view->nextItem = current($nextList->load());
    }

    /**
     * @return Zend_Paginator
     */
    protected function getPaginator()
    {
        $newsList = new News\Listing();
        $newsList->setOrderKey('published_date')
            ->setOrder('DESC');
        $paginator = \Zend_Paginator::factory($newsList);
        $paginator->setCurrentPageNumber( $this->_getParam('page') );
        $paginator->setItemCountPerPage(10);
        return $paginator;
    }
}
