<?php
$n = $this->news;
$featuredImageThumbnail = null;
if( $n->getImage() ) {
    $featuredImageThumbnail = $n->getImage()->getThumbnail(array(
        "width" => 513,
      //  "height" => 480,
      //  "cover" => true
    ));
}
?>
<article class="news-feed-list-item">
    <a href="<?= $this->url([ 'key' => $n->getKey() ], 'news-detail'); ?>">
        <div class="news-feed-list-item-image" style="background:transparent url(<?= $featuredImageThumbnail; ?>) no-repeat center center;background-size:cover;"></div>
        <div class="non-image">
            <h2 class="news-feed-list-item-title"><?= $n->getTitle(); ?></h2>
            <h4 class="news-feed-list-item-publication-date">Published <span class="publication-date"><?= $n->getPublished_date()->format('M j, Y'); ?></span></h4>
            <div class="news-feed-list-item-summary"><?= $n->getSummary(); ?></div>
        </div>
    </a>
</article>
