<?php
if( $this->images ):
    $this->headScript()->appendFile('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js');
    $this->headScript()->appendFile('https://npmcdn.com/masonry-layout@4.1/dist/masonry.pkgd.min.js');
    $this->headScript()->appendFile('https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js');

    $imageCount = 0;
    $galleryTitle = isset($this->galleryTitle) ? $this->galleryTitle : 'Image Gallery';
    ?>
<div class="image-gallery">
    <?php foreach ($this->images as $image): ?>
        <a class="fb-link" rel="group1" href="<?php echo $image->getFullPath();
            ?>"><img src="<?= $image->getThumbnail(['width' => 350, 'aspectratio' => true])
            ?>" id="news<?= $imageCount ?>"></a>
        <?php
        $imageCount++;
        if ($imageCount == 3) {
            break;
        }
    endforeach;
    ?>
    <a class="gallery-link" data-toggle="modal" data-target="#imageModal">View entire gallery</a>

    <!-- Modal -->
    <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="newsImageModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <h4 class="modal-title" id="newsImageModal"><?= $galleryTitle ?></h4>

                    <div id="masonry-container">
                        <div class="grid">
                            <div class="grid-sizer">
                                <?php foreach ($this->images as $image): ?>
                                    <div class="grid-item">
                                        <a class="fb-link" rel="group1" href="<?php echo $image->getFullPath();
                                            ?>"><img src="<?php echo $image->getThumbnail(array('width' => 350, 'aspectratio' => true))
                                            ?>" id="news<?= $imageCount ?>"></a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        var $container = $('#imageModal');
        // initialize Masonry after all images have loaded
        $container.on('shown.bs.modal',function () {
            $('.grid').masonry({
                // options
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
            });
        });

        /* Apply fancybox to multiple items */
        $("a.fb-link").fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200,
            'overlayShow': false
        });
    });
</script>
<?php endif; ?>
