<?= $this->areablock('pre-container'); ?>
    <div class="container">
        <?= $this->areablock('pre-news'); ?>

        <?php if( count($this->paginator) > 0 ): ?>
            <section class="news-feed">
                <div class="news-feed-list">
                    <?php foreach( $this->paginator as $news ): ?>
                        <?php $this->template( 'templates/partial/newsFeedItem.php', ['news' => $news, 'coming' => true] ); ?>
                    <?php endforeach; ?>
                </div>
                <!-- pagination start -->
                <?= $this->paginationControl($this->comingPaginator, 'Sliding', 'templates/paging.php', [
                    'urlprefix' => '/news/coming?page=',
                    'appendQueryString' => true
                ]); ?>
                <!-- pagination end -->
            </section>
        <?php else: ?>
            <p class="no-results-message">No news yet. Check back later!</p>
        <?php endif; ?>

        <?= $this->areablock('post-news'); ?>
    </div>
<?= $this->areablock('post-container'); ?>