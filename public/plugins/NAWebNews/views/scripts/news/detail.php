<?php
$n = $this->newsItem;
$featuredImageThumbnail = null;
if( $n->getImage() ) {
    $featuredImageThumbnail = $n->getImage()->getThumbnail(array(
        "width" => 980,
        "height" => 400,
        "cover" => true
    ));
}

function naweb_truncate( $str, $len = 25, $tail = '&hellip;' )
{
    if( strlen( $str ) <= $len ){
        return $str;
    }

    return substr( $str, 0, $len ) . $tail;
}
?>
<article class="full-article news-detail">
    <header class="full-article-header news-detail-header">
        <h2 class="full-article-title news-detail-title"><?= $n->getTitle(); ?></h2>
        <h4 class="full-article-publication-date">Published <?= $n->getPublished_date()->format('M j, Y'); ?></h4>
        <?php if( $featuredImageThumbnail ): ?>
            <img class="full-article-featured-image news-detail-featured" src="<?= $featuredImageThumbnail ?>" alt="" />
        <?php endif; ?>
    </header>

    <div class="full-article-body news-detail-body">
        <?= $n->getContent(); ?>
    </div>

    <?php
    if ($n->getImages()){
        echo $this->template('templates/image-gallery.php', ['images' => $n->getImages()]);
    }
    ?>

    <nav class="full-article-bottom-nav">
        <?php if( $this->prevItem ): ?>
            <a class="full-article-prev-item" href="<?=
                $this->url(['key' => $this->prevItem->getKey()], 'news-detail');
            ?>"><?= naweb_truncate( $this->prevItem->getTitle() ); ?></a>
        <?php endif; ?>
        <?php if( $this->nextItem ): ?>
            <a class="full-article-next-item" href="<?=
            $this->url(['key' => $this->nextItem->getKey()], 'news-detail');
            ?>"><?= naweb_truncate($this->nextItem->getTitle()); ?></a>
        <?php endif; ?>
        <a class="full-article-home-link" href="/news">News home</a>
    </nav>
</article>

