<?php

use Pimcore\Model\Object\News;

class Document_Tag_Area_Newshighlights extends \Pimcore\Model\Document\Tag\Area\AbstractArea
{

    public function action()
    {
        $this->view->setScriptPath(
            array_merge(
                $this->view->getScriptPaths(),
                array(
                    PIMCORE_PLUGINS_PATH . '/NAWebNews/views/scripts/'
                )
            )
        );

        $newsList = new News\Listing();
        $newsList->setOrderKey(['published_date', 'o_creationDate'])
            ->setOrder('DESC')
            ->setLimit(3);
        $newsItems = $newsList->load();

        $this->view->topItem = count($newsItems) ? array_shift($newsItems) : null;
        $this->view->newsItems = $newsItems;
    }

    /**
     * optional
     * Returns a custom html wrapper element (return an empty string if you don't want a wrapper element)
     */
    public function getBrickHtmlTagOpen($brick){
        return '';
    }

    /**
     * optional
     */
    public function getBrickHtmlTagClose($brick){
        return '';
    }

}