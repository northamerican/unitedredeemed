<?php if( $this->topItem ): ?>
<div class="announcement-teasers">
    <div class="announcement featured">
        <div class="img-wrapper">
            <?php if( $this->topItem->getImage() ): ?>
                <?= $this->topItem->getImage()->getThumbnail('news-teaser-featured')->getHtml([], ["width","height"]); ?>
            <?php else: ?>
                <i class="fa fa-newspaper-o" aria-hidden="true"></i>
            <?php endif; ?>
        </div>
        <div class="content">
            <h3><a href="<?= $this->url([ 'key' => $this->topItem->getKey() ], 'news-detail'); ?>"><?= $this->topItem->getTitle(); ?></a></h3>
            <div class="teaser-summary"><?= $this->topItem->getSummary(); ?></div>
        </div>
    </div>

    <?php
    if( $this->newsItems ):
        foreach( $this->newsItems as $newsItem ): ?>
            <div class="announcement">
                <div class="img-wrapper">
                    <?php if( $newsItem->getImage() ): ?>
                        <?= $newsItem->getImage()->getThumbnail('news-teaser-standard')->getHtml([], ["width","height"]); ?>
                    <?php else: ?>
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                    <?php endif; ?>
                </div>
                <div class="content">
                    <h3><a href="<?= $this->url([ 'key' => $newsItem->getKey() ], 'news-detail'); ?>"><?= $newsItem->getTitle(); ?></a></h3>
                </div>
            </div>
    <?php endforeach;
    endif; ?>
</div>
<?php endif; ?>