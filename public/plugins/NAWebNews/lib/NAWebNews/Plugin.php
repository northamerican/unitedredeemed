<?php

namespace NAWebNews;

use Pimcore\API\Plugin as PluginLib;
use NAWebNews\Plugin\Installer;
use Pimcore\Model\Object\ClassDefinition;

class Plugin extends PluginLib\AbstractPlugin implements PluginLib\PluginInterface
{
    public function init()
    {
        parent::init();

        // register your events here

        // using anonymous function
        \Pimcore::getEventManager()->attach("document.postAdd", function ($event) {
            // do something
            $document = $event->getTarget();
        });

        // using methods
        \Pimcore::getEventManager()->attach("document.postUpdate", [$this, "handleDocument"]);

        // for more information regarding events, please visit:
        // https://www.pimcore.org/docs/latest/Extending_Pimcore/Event_API_and_Event_Manager.html
        // http://framework.zend.com/manual/1.12/de/zend.event-manager.event-manager.html
        // https://www.pimcore.org/docs/latest/Extending_Pimcore/Plugin_Developers_Guide/Plugin_Class.html
    }

    public function handleDocument($event)
    {
        // do something
        $document = $event->getTarget();
    }

    public static function install()
    {
        try {
            $installer = new Installer(PIMCORE_PLUGINS_PATH . '/NAWebNews/install');
            $installer->createObjectFolder('news');
            $installer->importDocuments();
            //   $installer->importTranslations();
            $installer->createClass('News');
            $installer->addClassmap('Object_News', '\\News');

            $installer->createImageThumbnails();
            $installer->createStaticRoutes();
        } catch (\Exception $e) {
            \Logger::crit($e);
            self::uninstall(); // revert installation
            return $e->getMessage();
        }
        return 'Install successful';
    }

    public static function uninstall()
    {
        try {
            $installer = new Installer(PIMCORE_PLUGINS_PATH . '/NAWebNews/install');

            $installer->removeObjectFolder('/news');
            $installer->removeClassmap('Object_News');
            $installer->removeDocuments();
            $installer->removeClass('News');
            $installer->removeImageThumbnails();
            $installer->removeStaticRoutes();

        } catch (\Exception $e) {
            \Logger::crit($e);
            return $e->getMessage();
        }

        return 'Uninstallation was successful';
    }

    public static function isInstalled()
    {
        $eventClass = ClassDefinition::getByName('News');
        if ($eventClass) {
            return true;
        }
        return false;
    }
}
