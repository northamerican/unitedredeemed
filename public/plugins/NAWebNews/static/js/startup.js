pimcore.registerNS("pimcore.plugin.nawebnews");

pimcore.plugin.nawebnews = Class.create(pimcore.plugin.admin, {
    getClassName: function() {
        return "pimcore.plugin.nawebnews";
    },

    initialize: function() {
        pimcore.plugin.broker.registerPlugin(this);
    },
 
    pimcoreReady: function (params,broker){
        // alert("NAWebNews Plugin Ready!");
    }
});

var nawebnewsPlugin = new pimcore.plugin.nawebnews();

