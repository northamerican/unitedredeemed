<?php

use Pimcore\Model\Object\Event;

class Document_Tag_Area_ComingEvents extends \Pimcore\Model\Document\Tag\Area\AbstractArea
{

    public function action()
    {
        $this->view->setScriptPath(
            array_merge(
                $this->view->getScriptPaths(),
                array(
                    PIMCORE_PLUGINS_PATH . '/NAWebEvents/views/scripts/'
                )
            )
        );

    }

    public function getEventList($past = false, $limit = 4, $otherConditions = [])
    {
        $dateStr = 'IF(end, end >= UNIX_TIMESTAMP(CURDATE()), start >= UNIX_TIMESTAMP(CURDATE()))';
        if ($past) {
            $dateStr = 'IF(end, end <= UNIX_TIMESTAMP(CURDATE()), start <= UNIX_TIMESTAMP(CURDATE()))';
        }

        $conditions = array_merge($otherConditions, [
            $dateStr
        ]);



        $eventList = new Event\Listing();
        $eventList->setCondition(implode(' AND ', $conditions))
            ->setOrderKey("start")
            ->setOrder('ASC')
            ->setLimit($limit);
        $events = $eventList->load();
        return $events;
    }

    /**
     * optional
     * Returns a custom html wrapper element (return an empty string if you don't want a wrapper element)
     */
    public function getBrickHtmlTagOpen($brick){
        return '';
    }

    /**
     * optional
     */
    public function getBrickHtmlTagClose($brick){
        return '';
    }

}