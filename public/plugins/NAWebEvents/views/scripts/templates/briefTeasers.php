<ul class="event-list">
    <?php
    $now = new DateTime();
    if( $this->comingEvents ):
        foreach( $this->comingEvents as $event ): ?>
            <li>
                <a href="<?= $this->url([ 'key' => $event->getKey() ], 'event-detail'); ?>">
                    <h3><?= $event->getName(); ?></h3>
                    <?= $this->template('templates/dateRange.php', ['start' => $event->getStart(), 'end' => $event->getEnd(), 'isFuture' => $event->getStart() > $now ]); ?>
                </a>
            </li>
        <?php endforeach;
    else: ?>
        <li>
            <h3>No Events Scheduled</h3>
            <p>Check back later.</p>
        </li>
    <?php endif; ?>
</ul>