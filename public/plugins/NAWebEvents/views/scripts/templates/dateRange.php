<?php
// Set timezones
$this->start->setTimeZone( new DateTimeZone('America/New_York'));
if( $this->end ){
    $this->end->setTimeZone( new DateTimeZone('America/New_York'));
}


$start = $this->start;
$end = $this->end;
$isFuture = !empty($this->isFuture) && $this->isFuture;

$range = [
    'startDate' => $this->start->format('M d'),
    'endDate' => $this->end ? $this->end->format('M d') : null,
];
if( $this->start->format( 'H' ) !== '00' ){
    $range['startTime'] = $this->start->format( 'g:ia' );
}
if( $this->end && !empty($range['startTime']) ){
    $range['endTime'] = $this->end->format( 'g:ia' );
}


// No end
if( empty( $range['endDate'] ) ){ ?>
<span class="datetime no-end">
    <span class="start-date"><?php
        echo $isFuture ?  $this->start->format( 'D, ') . $range['startDate'] : $range['startDate' ] . ', '. $start->format('Y'); ?></span>
    <?php if( $isFuture && !empty( $range['startTime'] ) ): ?>
        <span class="words"> at </span>
        <span class="start-time"><?= $range['startTime']; ?></span>
    <?php endif; ?>
</span>
<?php
}

// Single day
elseif( $range['startDate'] === $range['endDate'] ){ ?>
<span class="datetime single-day">
    <span class="start-date"><?= $range['startDate']; ?></span>
    <?php if( $isFuture && !empty( $range['startTime'] ) ): ?>
        <span class="time-range"><?= sprintf( '%s&ndash;%s', $range['startTime'], $range['endTime']); ?></span>
    <?php endif; ?>
</span>
<?php
}

// Day range
else {
    $endDate = ($start->format('Y') === $end->format('Y')) ? $end->format('d') : $range['endDate'];
    ?>
<span class="datetime range">
    <span class="start-date"><?= $range['startDate']; ?></span>&ndash;<span class="end-date"><?= $endDate ?></span>,
    <span class="year"><?= $this->end->format('Y'); ?></span>
</span>
<?php
}
