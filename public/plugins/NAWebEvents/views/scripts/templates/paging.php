<div class="paginator">
    <!-- Previous page link -->
    <?php if (isset($this->previous)): ?>
        <a class="prev-link dir-link" href="<?= $this->url(['page' => $this->previous]); ?>">Previous</a>
    <?php endif; ?>
    <!-- Numbered page links -->
    <?php foreach ($this->pagesInRange as $page): ?>
        <span class="page-number">
            <?php if ($page != $this->current): ?>
                <a href="<?= $this->url(['page' => $page]); ?>"><?= $page; ?></a>
            <?php else: ?>
                <?= $page; ?>
            <?php endif; ?>
        </span>
    <?php endforeach; ?>
    <!-- Next page link -->
    <?php if (isset($this->next)): ?>
        <a class="next-link dir-link" href="<?= $this->url(['page' => $this->next]); ?>">Next</a>
    <?php endif; ?>
</div>