<?php
$event = $this->event;
$city = '';
if( $event->getLocation() ) {
    $locationMeta = current($event->getLocation());
    $location = $locationMeta->getObject();
    $city = $locationMeta->getCity_override() ?: $location->getCity();
}
?>
<article class="event-feed-list-item<?php if( empty($this->coming) ) echo ' archive'; ?>">
    <div class="date">
        <?= $event->getStart()->format('M j'); ?>
        <div class="year"><?= $event->getStart()->format('Y'); ?></div>
    </div>

    <div class="non-date">
        <h2><a href="<?= $this->url([ 'key' => $event->getKey() ], 'event-detail'); ?>"><?= $event->getName(); ?></a></h2>
        <?php
        if ($event->getLocation()) :
            ?><div class="event-teaser-location"><?= $city ?></div><?php
        endif;
        ?>
    </div>
</article>
