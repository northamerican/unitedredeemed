<?php
$location = $this->location->getObject();
$mapPoint = $location->getPoint();

$address = $location->getAddress();
if( !$address ){
    $address = $location->getCity() ?: $this->location->getCity_override();
}
?>
<div class="location-detail">
    <div class="location-venue-name"><?= $location->getVenuename() ?></div>
    <div class="location-address"><?= nl2br($address) ?></div>
    <?php if ($this->extraInfo) : ?>
        <div class="location-extra"><?= $this->extraInfo; ?></div>
    <?php endif; ?>
    <?php if( $mapPoint ): ?>
        <a target="_blank" href="https://maps.google.com/maps?t=m&q=<?= $mapPoint->getLatitude(); ?>%2C<?= $mapPoint->getLongitude(); ?>">Map</a>
    <?php endif; ?>
</div>