<?= $this->areablock('pre-container'); ?>
<div class="container">
    <?= $this->areablock('pre-events'); ?>

    <?php if( count($this->comingPaginator) > 0 ): ?>
        <div class="event-feed">
            <div class="event-feed-list">
                <?php foreach( $this->comingPaginator as $event ): ?>
                    <?php $this->template( 'templates/partial/eventFeedItem.php', ['event' => $event, 'coming' => true] ); ?>
                <?php endforeach; ?>
            </div>
            <!-- pagination start -->
            <?= $this->paginationControl($this->comingPaginator, 'Sliding', 'templates/paging.php', [
                'urlprefix' => '/events/coming?page=',
                'appendQueryString' => true
            ]); ?>
            <!-- pagination end -->
        </div>
    <?php else: ?>
        <p class="no-results-message">No future events currently planned.</p>
    <?php endif; ?>

    <?= $this->areablock('between-event-types'); ?>

    <?php if( count($this->archivedPaginator) > 0 ): ?>
        <div class="events-archive">
            <div class="event-feed-list">
                <?php foreach( $this->archivedPaginator as $event ): ?>
                    <?php $this->template( 'templates/partial/eventFeedItem.php', ['event' => $event, 'coming' => false] ); ?>
                <?php endforeach; ?>
            </div>
            <!-- pagination start -->
            <?= $this->paginationControl($this->archivedPaginator, 'Sliding', 'templates/paging.php', [
                'urlprefix' => '/events/archive??page=',
                'appendQueryString' => true
            ]); ?>
            <!-- pagination end -->
        </div>
    <?php endif; ?>

    <?= $this->areablock('post-events'); ?>
</div>
<?= $this->areablock('post-container'); ?>