<?= $this->areablock('pre-container'); ?>
    <div class="container">
        <?= $this->areablock('pre-events'); ?>

        <?php if( count($this->paginator) > 0 ): ?>
            <section class="event-feed">
                <div class="event-feed-list">
                    <?php
                    foreach( $this->paginator as $event ){
                        $this->template( 'templates/partial/eventFeedItem.php', ['event' => $event, 'coming' => false] );
                    }
                    ?>
                </div>
                <!-- pagination start -->
                <?= $this->paginationControl($this->paginator, 'Sliding', 'templates/paging.php', [
                    'urlprefix' => $this->document->getFullPath() . '?page=',
                    'appendQueryString' => true
                ]); ?>
                <!-- pagination end -->
            </section>
        <?php endif; ?>

        <?= $this->areablock('post-events'); ?>
    </div>
<?= $this->areablock('post-container'); ?>