<?php
$e = $this->event;
$featuredImageThumbnail = null;
if( $e->getImage() ) {
    $featuredImageThumbnail = $e->getImage()->getThumbnail(array(
        'height' => 360,
        'aspectratio' => true
    ));
}

function naweb_truncate( $str, $len = 25, $tail = '&hellip;' )
{
    if( strlen( $str ) <= $len ){
        return $str;
    }

    return substr( $str, 0, $len ) . $tail;
}
?>
<article class="full-article event-detail">

    <header class="full-article-header event-detail-header">

        <?php if( $featuredImageThumbnail ): ?>
        <img src="<?= $featuredImageThumbnail ?>" alt="" class="event-detail-featured" />
        <?php endif; ?>

        <h2 class="full-article-title event-detail-title"><?= $e->getName(); ?></h2>

        <div class="event-prime-info">
            <div class="event-time-and-place">
                <?= $this->template('templates/dateRange.php', ['start' => $e->getStart(), 'end' => $e->getEnd(), 'isFuture' => $e->getStart() > new \DateTime()]); ?>
                <?php
                if ($e->getLocation()) {
                    echo $this->template('templates/location.php', ['location' => current($e->getLocation()), 'extraInfo' => $e->getLocation_info()]);
                }
                ?>
            </div>
        </div>
    </header>

    <div class="full-article-body event-detail-description">
        <?= $e->getDescription(); ?>
    </div>

    <nav class="full-article-bottom-nav">
        <?php if( $this->prevItem ): ?>
            <a class="full-article-prev-item" href="<?=
            $this->url(['key' => $this->prevItem->getKey()], 'event-detail');
            ?>"><?= naweb_truncate( $this->prevItem->getName() ); ?></a>
        <?php endif; ?>
        <?php if( $this->nextItem ): ?>
            <a class="full-article-next-item" href="<?=
            $this->url(['key' => $this->nextItem->getKey()], 'event-detail');
            ?>"><?= naweb_truncate($this->nextItem->getName()); ?></a>
        <?php endif; ?>
        <a class="full-article-home-link" href="/events">Events home</a>
    </nav>
</article>

