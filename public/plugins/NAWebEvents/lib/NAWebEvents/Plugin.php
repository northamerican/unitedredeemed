<?php

namespace NAWebEvents;

use Pimcore\API\Plugin as PluginLib;
use NAWebEvents\Plugin\Installer;
use Pimcore\Model\Object\ClassDefinition;

class Plugin extends PluginLib\AbstractPlugin implements PluginLib\PluginInterface
{
    public function init()
    {
        parent::init();

    }

    public function handleDocument($event)
    {
        // do something
        $document = $event->getTarget();
    }

    public static function install()
    {
        try {
            $installer = new Installer(PIMCORE_PLUGINS_PATH . '/NAWebEvents/install');
            $installer->createObjectFolder('events');
            $installer->createObjectFolder('locations');
            $installer->importDocuments();
            //   $installer->importTranslations();
            $installer->createClass('Location');
            $installer->addClassmap('Object_Location', '\\Location');

            $installer->createClass('Event');
            $installer->addClassmap('Object_Event', '\\Event');

            $installer->createStaticRoutes();
        } catch (\Exception $e) {
            \Logger::crit($e);
            self::uninstall(); // revert installation
            return $e->getMessage();
        }
        return 'Install successful';
    }

    public static function uninstall()
    {
        try {
            $installer = new Installer(PIMCORE_PLUGINS_PATH . '/NAWebEvents/install');

            $installer->removeObjectFolder('/events');
            $installer->removeObjectFolder('/locations');
            $installer->removeClassmap('Object_Event');
            $installer->removeClassmap('Object_Location');
            $installer->removeDocuments();
            $installer->removeClass('Event');
            $installer->removeClass('Location');

        } catch (\Exception $e) {
            \Logger::crit($e);
            return $e->getMessage();
        }

        return 'Uninstallation was successful';
    }

    public static function isInstalled()
    {
        $eventClass = ClassDefinition::getByName('Event');
        if ($eventClass) {
            return true;
        }
        return false;
    }
}
