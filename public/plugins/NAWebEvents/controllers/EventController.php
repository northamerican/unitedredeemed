<?php

use Pimcore\Model\Object\Event;
use NAWebEvents\Controller\Action;

class NAWebEvents_EventController extends Action
{

    /**
     * Default action
     */
    public function defaultAction()
    {
        $this->view->comingPaginator  = $this->getPaginator(true);
        $this->view->archivedPaginator  = $this->getPaginator();
    }

    /**
     * Event detail action
     */
    public function detailAction()
    {
        $this->enableLayout();

        $key = $this->getParam('key');
        $eventList = new Event\Listing();
        $eventList->setCondition('o_key=?', $key);
        $eventList->setLimit(1);
        $eventItem = current($eventList->load());
        $this->view->event = $eventItem;

        // Previous item
        $prevList = new Event\Listing();
        $prevList->setCondition('IF(start = ?, o_creationDate < ?, start < ?)',
            [
                $eventItem->getStart()->timestamp,
                $eventItem->getCreationDate(),
                $eventItem->getStart()->timestamp,
            ]);
        $prevList->setOrderKey(['start', 'o_creationDate'])
            ->setOrder('DESC')
            ->setLimit(1);
        $this->view->prevItem = current($prevList->load());

        // Next item
        $nextList = new Event\Listing();
        $nextList->setCondition('IF(start = ?, o_creationDate > ?, start > ?)',
            [
                $eventItem->getStart()->timestamp,
                $eventItem->getCreationDate(),
                $eventItem->getStart()->timestamp,
            ]);
        $nextList->setOrderKey(['start', 'o_creationDate'])
            ->setOrder('ASC')
            ->setLimit(1);
        $this->view->nextItem = current($nextList->load());

    }

    /**
     * Event feed page for archived events
     */
    public function archiveAction()
    {
        $this->view->paginator = $this->getPaginator(true);
    }

    /**
     * Event feed page for coming events
     */
    public function comingAction()
    {
        $this->view->paginator  = $this->getPaginator();
    }

    /**
     * @param bool $coming
     * @return Zend_Paginator
     */
    protected function getPaginator( $coming = false )
    {
        $eventOrder = $coming ? 'ASC' : 'DESC';

        $eventList = $this->getEventList(!$coming);
        $eventList->setOrderKey('start');
        $eventList->setOrder($eventOrder);
        $paginator = \Zend_Paginator::factory($eventList);
        $paginator->setCurrentPageNumber( $this->_getParam('page') );
        $paginator->setItemCountPerPage(10);
        return $paginator;
    }

    /**
     * @param bool $past
     * @param null|bool $isFeatured
     * @param array $otherConditions
     * @return Object_Event_List
     */
    protected function getEventList( $past = false, $otherConditions = [] ){
        $dateStr = 'IF(end, end >= UNIX_TIMESTAMP(CURDATE()), start >= UNIX_TIMESTAMP(CURDATE()))';
        if( $past ){
            $dateStr = 'IF(end, end <= UNIX_TIMESTAMP(CURDATE()), start <= UNIX_TIMESTAMP(CURDATE()))';
        }

        $conditions = array_merge( $otherConditions, [
            $dateStr
        ]);


        $eventList = new Object_Event_List();
        $eventList->setCondition(implode(' AND ', $conditions))
            ->setOrderKey("start")
            ->setOrder('DESC');

        return $eventList;
    }
}
