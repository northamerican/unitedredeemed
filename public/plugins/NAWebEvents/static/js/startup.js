pimcore.registerNS("pimcore.plugin.nawebevents");

pimcore.plugin.nawebevents = Class.create(pimcore.plugin.admin, {
    getClassName: function() {
        return "pimcore.plugin.nawebevents";
    },

    initialize: function() {
        pimcore.plugin.broker.registerPlugin(this);
    },
 
    pimcoreReady: function (params,broker){
        // alert("NAWebEvents Plugin Ready!");
    }
});

var nawebeventsPlugin = new pimcore.plugin.nawebevents();

