var gulp = require('gulp');
var postcss = require('gulp-postcss');
var shortColor = require('postcss-short-color');
var autoprefixer = require('autoprefixer');
var flexbugs = require('postcss-flexbugs-fixes');
var cssnano = require('cssnano');
var atImport = require("postcss-import");
var cssNesting = require('postcss-nesting');
var verticalRhythm = require('postcss-vertical-rhythm');
var ext_replace = require('gulp-ext-replace');
var rename = require("gulp-rename");


var browserSync = require('browser-sync').create(); // create a browser sync instance.
var stylePlugins = [
    atImport,
    require('postcss-advanced-variables'),
    cssNesting,
    require("postcss-custom-media"),
    require('postcss-short-font-size'),
    require('postcss-sass-color-functions'),
    require('lost'),
    verticalRhythm,
    flexbugs,
    shortColor,
    autoprefixer({browsers: ['last 2 versions'], cascade: false}),
    cssnano
];

gulp.task('base-styles', function () {
    return gulp.src('./pcss/style.pcss')
        .pipe(postcss(stylePlugins))
        .pipe(ext_replace('.min.css', '.pcss'))
        .pipe(gulp.dest('./public/css'))
        .pipe(browserSync.stream());
});

gulp.task('plugin-styles', function () {
    gulp.start('base-styles');
    return gulp.src('./public/plugins/*/static/pcss/style.pcss')
        .pipe(postcss(stylePlugins))
        .pipe(rename(function(path){
            path.extname = ".min.css";
            path.dirname = path.dirname.replace(/\/pcss/, "/css");;
        }))
        .pipe(gulp.dest('./public/plugins'))
        .pipe(browserSync.stream());

});

gulp.task('views', function(){
   browserSync.reload();
});

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "ur.dev"
    });
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('public/**/views/**/*.php', ['views'])
    gulp.watch('pcss/**/*.pcss', ['base-styles']);
    gulp.watch('public/plugins/**/static/pcss/**/*.pcss', ['plugin-styles']);
});

// Default Task
gulp.task('default', ['browser-sync', 'base-styles', 'plugin-styles', 'watch']);